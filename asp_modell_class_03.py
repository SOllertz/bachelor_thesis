# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 13:22:35 2019

@author: Sascha
"""

#import modelbase
import numpy as np
from modelbase.ode import Model, Simulator

par={
            "AK1":0.25,
            "AK2":0.25,
            "AKHSDHI":0.63,
            "AKHSDHII":0.63,
            "ASADH":11.6,
            "DHDPS1":1.6,
            "DHDPS2":1.6,
            "HSK":4,
            "TS1":7.4,
            "TD":0.36,
            "CGS":0.7,
            "THA":0,
            "LKR":0,
            "AdoMet":20,
            "Cys":15,
            "Phosphate":10000,
            "Val":100,
            "AK1_kforward_app_exp":5.65,
            "AK1_kreverse_app_exp":1.57,
            "AK1_Lys_Ki_app_exp":550,
            "AK1_AdoMet_Ka_app_exp":3.5,
            "AK1_h_exp":2,
            "AK2_kforward_app_exp":3.15,
            "AK2_kreverse_app_exp":0.88,
            "AK2_Lys_Ki_app_exp":22,
            "AK2_h_exp":1.1,
            "AKI_kforward_app_exp":0.36,
            "AKI_kreverse_app_exp":0.10,
            "AKI_Thr_Ki_app_exp":124,
            "AKI_h_exp":2,
            "AKII_kforward_app_exp":1.35,
            "AKII_kreverse_app_exp":0.38,
            "AKII_Thr_Ki_app_exp":109,
            "AKII_h_exp":2,    
            "ASADH_kforward_app_exp":0.9,
            "ASADH_kreverse_app_exp":0.23,
            "DHDPS1_k_app_exp":1,
            "DHDPS1_Lys_Ki_app_exp":10,
            "DHDPS1_h_exp":2,
            "DHDPS2_k_app_exp":1,
            "DHDPS2_Lys_Ki_app_exp":33,
            "DHDPS2_h_exp":2,
            "HSDHI_kforward_app_exp":0.84,
            "HSDHI_Thr_relative_residual_activity_app_exp":0.15,
            "HSDHI_Thr_relative_inhibition_app_exp":0.85,
            "HSDHI_Thr_Ki_app_exp":400,
            "HSDHII_kforward_app_exp":0.64,
            "HSDHII_Thr_relative_residual_activity_app_exp":0.25,
            "HSDHII_Thr_relative_inhibition_app_exp":0.75,
            "HSDHII_Thr_Ki_app_exp":8500,
            "HSK_kcat_app_exp":2.8,
            "HSK_Hser_app_exp":14,    
            "TS1_kcatmin_exp":0.42,
            "TS1_AdoMet_kcatmax_exp":3.5,
            "TS1_AdoMEt_Km_no_AdoMet_exp":250,
            "TS1_AdoMet_Ka1_exp":73,
            "TS1_AdoMet_Ka2_exp":0.5,
            "TS1_AdoMet_Ka3_exp":1.09,
            "TS1_AdoMet_Ka4_exp":142,
            "TS1_Phosphate_Ki_exp":1000,
            "TS1_h_exp":2,
            "CGS_kcat_exp":30,
            "CGS_Cys_Km_exp":460,
            "CGS_Phser_Km_exp":2500,
            "CGS_Phosphate_Ki_exp":2000,
            "TD_k_app_exp":0.0124,
            "TD_Ile_Ki_no_Val_app_exp":30,
            "TD_Val_Ka1_app_exp":73,
            "TD_Val_Ka2_app_exp":615,    
            "TD_h_app_exp":3,
            "Lys_tRNAS_Vmax":0.43,
            "Lys_tRNAS_Lys_Km":25,
            "Thr_tRNAS_Vmax":0.43,
            "Thr_tRNAS_Thr_Km":100,
            "Ile_tRNAS_Vmax":0.43,
            "Ile_tRNAS_Ile_Km":20,
            "THA_kcat_exp":1.7,
            "THA_Thr_Km_exp":7100,
            "LKR_kcat_exp":3.1,
            "LKR_Lys_Km_exp":13000
            }

class asp_modell(Model):
    
    def __init__(self,pars={}):
        
        defpar=par.copy()
    
        super(asp_modell,self).__init__(defpar)
        
        cpd_names=["AspP","ASA","Lys","Hser","Phser","Thr","Ile"]

        self.update_parameters(pars)
        
        self.add_compounds(cpd_names)
         
        
        def Vak1(p,AspP,Lys):
            e=p.AK1*(p.AK1_kforward_app_exp - p.AK1_kreverse_app_exp*AspP)/(1+(Lys/(p.AK1_Lys_Ki_app_exp/(1+p.AdoMet/ p.AK1_AdoMet_Ka_app_exp)))**p.AK1_h_exp)
            return(e)
          
        def Vak2(p,AspP,Lys):
            e=p.AK2*(p.AK2_kforward_app_exp - p.AK2_kreverse_app_exp*AspP)/(1+(Lys/ p.AK2_Lys_Ki_app_exp)**p.AK2_h_exp)
            return(e)
            
        def VakI(p,AspP,Thr):
            e=p.AKHSDHI*(p.AKI_kforward_app_exp - p.AKI_kreverse_app_exp*AspP)*1/(1+(Thr/ p.AKI_Thr_Ki_app_exp)**p.AKI_h_exp)
            return(e)
        
        def VakII(p,AspP,Thr):
            e=p.AKHSDHII*(p.AKII_kforward_app_exp - p.AKII_kreverse_app_exp*AspP)/(1+(Thr/ p.AKII_Thr_Ki_app_exp)**p.AKII_h_exp)
            return(e)
            
        def Vasadh(p,AspP,ASA):
            e=p.ASADH*(p.ASADH_kforward_app_exp*AspP- p.ASADH_kreverse_app_exp*ASA)
            return(e)
           
        def Vdhdps1(p,ASA,Lys):
            e=p.DHDPS1* p.DHDPS1_k_app_exp *ASA*(1/(1+(Lys/p.DHDPS1_Lys_Ki_app_exp)**p.DHDPS1_h_exp))
            return(e)
        
        def Vdhdps2(p,ASA,Lys):
            e=p.DHDPS2* p.DHDPS2_k_app_exp *ASA*(1/(1+(Lys/ p.DHDPS2_Lys_Ki_app_exp)**p.DHDPS2_h_exp))
            return(e)
           
        def VhsdhI(p,ASA,Thr):
            e=p.AKHSDHI* p.HSDHI_kforward_app_exp *ASA*( p.HSDHI_Thr_relative_residual_activity_app_exp+p.HSDHI_Thr_relative_inhibition_app_exp /(1+Thr/ p.HSDHI_Thr_Ki_app_exp))
            return(e)
          
        def VhsdhII(p,ASA,Thr):
            e=p.AKHSDHII* p.HSDHII_kforward_app_exp *ASA*( p.HSDHII_Thr_relative_residual_activity_app_exp+p.HSDHII_Thr_relative_inhibition_app_exp /(1+Thr/ p.HSDHII_Thr_Ki_app_exp))
            return(e)   
            
        def Vhsk(p,Hser):
            e=p.HSK* p.HSK_kcat_app_exp *Hser/( p.HSK_Hser_app_exp +Hser)
            return(e)
           
        def Vts1(p,Phser):
            e=p.TS1*(p.TS1_kcatmin_exp + p.TS1_AdoMet_kcatmax_exp *p.AdoMet**p.TS1_h_exp /p.TS1_AdoMet_Ka1_exp)/(1+p.AdoMet**p.TS1_h_exp / p.TS1_AdoMet_Ka1_exp)*Phser/((1+p.Phosphate/p.TS1_Phosphate_Ki_exp)*( p.TS1_AdoMEt_Km_no_AdoMet_exp *(1+p.AdoMet/ p.TS1_AdoMet_Ka2_exp)/(1+p.AdoMet/ p.TS1_AdoMet_Ka3_exp))/(1+p.AdoMet**p.TS1_h_exp / p.TS1_AdoMet_Ka4_exp)+Phser)
            return(e)
           
        def Vcgs(p,Phser):
            e=p.CGS*( p.CGS_kcat_exp/(1+ p.CGS_Cys_Km_exp /p.Cys))*Phser/(( p.CGS_Phser_Km_exp /(1+ p.CGS_Cys_Km_exp /p.Cys))*(1+p.Phosphate/ p.CGS_Phosphate_Ki_exp)+ Phser)
            return(e)
            
        def Vtd(p,Thr,Ile):
            e=p.TD*p.TD_k_app_exp*Thr/(1+(Ile/( p.TD_Ile_Ki_no_Val_app_exp + p.TD_Val_Ka1_app_exp *p.Val/( p.TD_Val_Ka2_app_exp +p.Val)))** p.TD_h_app_exp)
            return(e)
           
        def VLys_tRNAS(p,Lys):
            e=p.Lys_tRNAS_Vmax *Lys/(p.Lys_tRNAS_Lys_Km+Lys)
            return(e)
        
        def VThr_tRNAS(p,Thr):
            e=p.Thr_tRNAS_Vmax *Thr/(p.Thr_tRNAS_Thr_Km+Thr)
            return(e)
            
        def VIle_tRNAS(p,Ile):
            e=p.Ile_tRNAS_Vmax *Ile/(p.Ile_tRNAS_Ile_Km+Ile)
            return(e)
        
        def Vtha(p,Thr):
            e=p.THA*p.THA_kcat_exp*Thr/(p.THA_Thr_Km_exp+Thr)
            return(e)
        
        def Vlkr(p,Lys):
            e=p.LKR*p.LKR_kcat_exp*Lys/(p.LKR_Lys_Km_exp+Lys)
            return(e)
        
        self.add_reaction("Vak1",Vak1,{"AspP":1},["AspP","Lys"])    
        self.add_reaction("Vak2",Vak2,{"AspP":1},["AspP","Lys"])
        self.add_reaction("VakI",VakI,{"AspP":1},["AspP","Thr"])
        self.add_reaction("VakII",VakII,{"AspP":1},["AspP","Thr"])
        self.add_reaction("Vasadh",Vasadh,{"ASA":1,"AspP":-1},["AspP","ASA"])
        self.add_reaction("Vdhdps1",Vdhdps1,{"ASA":-1,"Lys":1},["ASA","Lys"])
        self.add_reaction("Vdhdps2",Vdhdps2,{"ASA":-1,"Lys":1},["ASA","Lys"])
        self.add_reaction("VhsdhI",VhsdhI,{"ASA":-1,"Hser":1},["ASA","Thr"])
        self.add_reaction("VhsdhII",VhsdhII,{"ASA":-1,"Hser":1},["ASA","Thr"])
        self.add_reaction("Vhsk",Vhsk,{"Hser":-1,"Phser":1},["Hser"])
        self.add_reaction("Vts1",Vts1,{"Phser":-1,"Thr":1},["Phser"])
        self.add_reaction("Vcgs",Vcgs,{"Phser":-1},["Phser"])
        self.add_reaction("Vtd",Vtd,{"Thr":-1,"Ile":1},["Thr","Ile"])
        self.add_reaction("VLys_tRNAS",VLys_tRNAS,{"Lys":-1},["Lys"])
        self.add_reaction("VThr_tRNAS",VThr_tRNAS,{"Thr":-1},["Thr"])
        self.add_reaction("VIle_tRNAS",VIle_tRNAS,{"Ile":-1},["Ile"])
        self.add_reaction("Vtha",Vtha,{"Thr":-1},["Thr"])
        self.add_reaction("Vlkr",Vlkr,{"Lys":-1},["Lys"])
        
        

