# -*- coding: utf-8 -*-
"""
Created on Mon May 27 10:51:49 2019

@author: Sascha
"""

import modelbase
import matplotlib.pyplot as plt
import numpy as np
import pickle 
from asp_modell_class_03 import asp_modell
from modelbase.ode import Model,Simulator

cpd_names=["AspP","ASA","Lys","Hser","Phser","Thr","Ile"]

init=[0,0,0,0,0,0,0]

m_v1=asp_modell()

m_v1.remove_reactions("VLys_tRNAS")
m_v1.remove_reactions("VThr_tRNAS")
m_v1.remove_reactions("VIle_tRNAS")

m_v1.add_parameters({"P_Vmax":0.678})
m_v1.add_parameters({"P_Km": 24.9266})

def V_P(p,Lys,Thr,Ile):
    
    e=p.P_Vmax*(Lys/(p.P_Km + Lys))*(Thr/(p.P_Km + Thr))*(Ile/(p.P_Km + Ile))
    
    return(e)

m_v1.add_reaction("V_P",V_P,{"Lys":-1,"Thr":-1,"Ile":-1},["Lys","Thr","Ile"])

s_v1=Simulator(m_v1)

s_v1.set_initial_conditions(dict(zip(cpd_names,init)))

trash,s_v1_results=s_v1.simulate(time_points=np.linspace(0,10000,100000))

s_v1.plot()

#%% Modellierung des Lys, Thr und Ile Verbrauchs
dict_V_P={"Lys":-1,"Thr":-1,"Ile":-1}
ls_keys=list(dict_V_P.keys())
c=70

Plots,zahl=plt.subplots(3,2,figsize=(20,15))
p=zahl.ravel()
ls_legends=["Lys outflux (Thr=Ile=1)","Thr outflux (Lys=Ile=1)","Ile outflux (Lys=Thr=1)"]
ls_xlabel=["Lys/1","Thr/1","Ile/1"]
for j in ls_keys:
    dict_V_P={"Lys":-1,"Thr":-1,"Ile":-1}
    ls=[]
    a=0
    for i in np.linspace(0,c,c*10):
        dict_V_P[j]=-i
        m_v1.add_reaction("V_P",V_P,dict_V_P,["Lys","Thr","Ile"])
    #    print(i)
    #    print(m_v1.get_stoichiometry_df())
        a=a+1
        if a==c/2:
            print(a)
        s_v1=Simulator(m_v1)
        
        s_v1.set_initial_conditions(dict(zip(cpd_names,init)))
    
        try:
            array=s_v1.simulate_to_steady_state(tolerance=1e-6,max_steps=100000,step_size=1)
    
        except:
            array=np.array([0,0,0,0,0,0,0],dtype=np.float)
            array.fill(np.nan)
        finally:
            ls.append(array)
    ls=np.array(ls)   
    
    if ls_keys.index(j) ==0:
        a=0
    elif ls_keys.index(j) ==1:
        a=2
    else:
        a=4
    
    for i in [2,4,5,6]:
        if i ==5:
            p[a].plot(np.linspace(0,c,c*10),ls[:,i],label=cpd_names[i],linestyle="--")
        else:
            p[a].plot(np.linspace(0,c,c*10),ls[:,i],label=cpd_names[i])
    if ls_keys.index(j) ==0:
        p[a].set_xlim(xmin=0,xmax=c)
    else:
        p[a].set_xlim(xmin=0,xmax=c)
    p[a].legend()
    p[a].set(ylabel=r"Steady-state $\mu$M")
    p[a].set(xlabel=ls_xlabel[ls_keys.index(j)])
    p[a].set_title(ls_legends[ls_keys.index(j)])
    

    
    for i in [0,1,3]:
        p[a+1].plot(np.linspace(0,c,c*10),ls[:,i],label=cpd_names[i])
    if ls_keys.index(j) ==0:
        p[a+1].set_xlim(xmin=0,xmax=c)
    else:
        p[a+1].set_xlim(xmin=0,xmax=c)
    p[a+1].set_ylim(ymin=0,ymax=1.8)
    p[a+1].legend()
    p[a+1].set(ylabel=r"Steady-state $\mu$M")
    p[a+1].set(xlabel=ls_xlabel[ls_keys.index(j)])
    p[a+1].set_title(ls_legends[ls_keys.index(j)])   
    
    with open('ls_outflux_'+ j +'.pickle', 'wb') as handle:
        pickle.dump(ls, handle, protocol=pickle.HIGHEST_PROTOCOL)

m_v1.add_reaction("V_P",V_P,{"Lys":-1,"Thr":-1,"Ile":-1},["Lys","Thr","Ile"])  

#%%

with open('ls_outflux_Lys.pickle', 'rb') as handle:
    ls_values = pickle.load(handle)
    values=dict(zip(cpd_names,np.transpose(ls_values)))
#%%
ls_legends=["Lys outflux (Thr=Ile=1)","Thr outflux (Lys=Ile=1)","Ile outflux (Lys=Thr=1)"]
ls_xlabel=["Lys/1","Thr/1","Ile/1"]
ls_pickle=['ls_outflux_Lys.pickle','ls_outflux_Thr.pickle','ls_outflux_Ile.pickle']
ls_fig_names=["Lys_outflux","Thr_outflux","Ile_outflux"]
xlimes=[10,60,60]

for i in range(3):     
    with open(ls_pickle[i], 'rb') as handle:
        ls_values = pickle.load(handle)
        values=dict(zip(cpd_names,np.transpose(ls_values)))
        Plots,zahl=plt.subplots(1,2,figsize=(20,7.5))
        p=zahl.ravel()
        p[0].plot(np.linspace(0,70,700),values["AspP"],label="AspP")
        p[0].plot(np.linspace(0,70,700),values["ASA"],label="ASA")
        p[0].plot(np.linspace(0,70,700),values["Hser"],label="Hser")
        p[0].set_xlabel(ls_xlabel[i],fontsize=22)
        p[0].set_ylabel(r"Concentration [$\mu$M]",fontsize=22)
        p[0].set_xlim(xmin=0, xmax=xlimes[i])
        p[0].set_ylim(ymin=0,ymax=1.7)
        p[0].tick_params(labelsize=16,direction="in")
        p[0].set_title(ls_legends[i],size=22)
        p[0].legend(loc="right",fontsize=16)
        
        p[1].plot(np.linspace(0,70,700),values["Lys"],label="Lys")
        p[1].plot(np.linspace(0,70,700),values["Phser"],label="Phser")
        p[1].plot(np.linspace(0,70,700),values["Thr"],linestyle="--",label="Thr")
        p[1].plot(np.linspace(0,70,700),values["Ile"],label="Ile")
        p[1].set_xlabel(ls_xlabel[i],fontsize=22)
        p[1].set_xlim(xmin=0, xmax=xlimes[i])
        p[0].set_ylim(ymin=0,ymax=1.7)
        p[1].tick_params(labelsize=16,direction="in")
        p[1].set_title(ls_legends[i],size=22)
        p[1].legend(fontsize=16)
        Plots.savefig(ls_fig_names[i]+".svg",bbox_inches="tight")
        plt.show()

#%%
def steady_states(par,range_par):
    ls_met=[]
    rates_dic=dict()
    a=1
    for i in range_par:
        m_v1.update_parameters({par:i})
        
        s_v1=Simulator(m_v1)
            
        s_v1.set_initial_conditions(dict(zip(cpd_names,init)))
    
        try:
            array=s_v1.simulate_to_steady_state(tolerance=1e-6,max_steps=100000,step_size=1)
            s_v1.set_initial_conditions(dict(zip(cpd_names,array)))
            rates_dic_neu=m_v1.get_rates(t=1,y_full=m_v1.get_full_concentration_dict(array))
            
            if a==1:
                rates_dic=rates_dic_neu.copy()
            
            elif a ==2:
                for j in list(rates_dic.keys()):
                    rates_dic[j]=[rates_dic[j],rates_dic_neu[j]]
            elif a > 2:
                for j in list(rates_dic.keys()):
                    rates_dic[j].append(rates_dic_neu[j])
            a+=1
                
        except:
            array=np.array([0,0,0,0,0,0,0],dtype=np.float)
            array.fill(np.nan)
        finally:
            ls_met.append(array)
    return(np.array(ls_met),rates_dic)

ls_met,rates_dic=steady_states("AdoMet",np.linspace(2,30,280))
#%%  
Plots,zahl=plt.subplots(2,2,figsize=(20,10))
p=zahl.ravel()
p[3].plot(np.linspace(2,30,280),ls_met[:,2],label="Lys")
p[3].plot(np.linspace(2,30,280),ls_met[:,4],label="Phser")
p[3].plot(np.linspace(2,30,280),ls_met[:,5],label="Thr",linestyle="--")
p[3].plot(np.linspace(2,30,280),ls_met[:,6],label="Ile")
p[3].legend()   
p[3].set(xlabel=r"AdoMet $\mu$M",ylabel=r"Concentration $\mu$M")
p[3].set_title("Steady-state concentrations")


p[1].plot(np.linspace(2,30,280),ls_met[:,1],label="ASA")
p[1].legend()   
p[1].set(ylabel=r"Concentration $\mu$M")
p[1].set_title("Steady-state concentrations")

p[0].plot(np.linspace(2,30,280),rates_dic["Vcgs"],label="Met branch")
p[0].plot(np.linspace(2,30,280),rates_dic["Vtd"],label="Ile branch")
p[0].plot(np.linspace(2,30,280),rates_dic["VThr_tRNAS"],label="Thr-> Protein",linestyle="--")
#p[0].plot(np.linspace(2,30,280),rates_dic["V_P"],label="Thr-> Protein",linestyle="--") #für outflux == V_P
p[0].legend()   
p[0].set(ylabel=r"Flux in $\mu$M/s")
p[0].set_title("Steady-state fluxes")

p[2].plot(np.linspace(2,30,280),rates_dic["Vak1"],label="AK1")
p[2].plot(np.linspace(2,30,280),rates_dic["Vak2"],label="Ak2")
p[2].plot(np.linspace(2,30,280),rates_dic["VakI"],label="AkI")
p[2].plot(np.linspace(2,30,280),rates_dic["VakII"],label="AkII")
p[2].legend()   
p[2].set(xlabel=r"AdoMet $\mu$M",ylabel=r"Flux in $\mu$M/s")
p[2].set_title("Steady-state fluxes")
Plots.savefig("SS_AdoMet.pdf")
plt.show()

#%%

